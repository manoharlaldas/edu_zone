class EduzonesController < ApplicationController
  before_action :set_eduzone, only: [:show, :edit, :update, :destroy]

  # GET /eduzones
  # GET /eduzones.json
  def index
    @eduzones = Eduzone.all
  end

  # GET /eduzones/1
  # GET /eduzones/1.json
  def show
  end

  # GET /eduzones/new
  def new
    @eduzone = Eduzone.new
  end

  # GET /eduzones/1/edit
  def edit
  end

  # POST /eduzones
  # POST /eduzones.json
  def create
    @eduzone = Eduzone.new(eduzone_params)

    respond_to do |format|
      if @eduzone.save
        format.html { redirect_to @eduzone, notice: 'Eduzone was successfully created.' }
        format.json { render :show, status: :created, location: @eduzone }
      else
        format.html { render :new }
        format.json { render json: @eduzone.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eduzones/1
  # PATCH/PUT /eduzones/1.json
  def update
    respond_to do |format|
      if @eduzone.update(eduzone_params)
        format.html { redirect_to @eduzone, notice: 'Eduzone was successfully updated.' }
        format.json { render :show, status: :ok, location: @eduzone }
      else
        format.html { render :edit }
        format.json { render json: @eduzone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eduzones/1
  # DELETE /eduzones/1.json
  def destroy
    @eduzone.destroy
    respond_to do |format|
      format.html { redirect_to eduzones_url, notice: 'Eduzone was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eduzone
      @eduzone = Eduzone.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def eduzone_params
      params[:eduzone]
    end
end
