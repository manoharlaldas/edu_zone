json.array!(@eduzones) do |eduzone|
  json.extract! eduzone, :id
  json.url eduzone_url(eduzone, format: :json)
end
