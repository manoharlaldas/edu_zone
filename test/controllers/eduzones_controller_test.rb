require 'test_helper'

class EduzonesControllerTest < ActionController::TestCase
  setup do
    @eduzone = eduzones(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:eduzones)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create eduzone" do
    assert_difference('Eduzone.count') do
      post :create, eduzone: {  }
    end

    assert_redirected_to eduzone_path(assigns(:eduzone))
  end

  test "should show eduzone" do
    get :show, id: @eduzone
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @eduzone
    assert_response :success
  end

  test "should update eduzone" do
    patch :update, id: @eduzone, eduzone: {  }
    assert_redirected_to eduzone_path(assigns(:eduzone))
  end

  test "should destroy eduzone" do
    assert_difference('Eduzone.count', -1) do
      delete :destroy, id: @eduzone
    end

    assert_redirected_to eduzones_path
  end
end
